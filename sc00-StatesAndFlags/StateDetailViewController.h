//
//  StateDetailViewController.h
//  sc00-StatesAndFlags
//
//  Created by Entec Department on 11/2/16.
//  Copyright © 2016 COP2654. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "States.h"
#import "StatesTablleViewController.h"
#import "StateTableViewCell.h"
#import "StateDetailViewController.h"
#import "SearchResultsTableViewController.h"

@interface StateDetailViewController : UIViewController
@property (strong, nonatomic)States* myState;
@end
